from os import getenv
from typing import Dict
from pyrogram import Client, filters, idle
from pyrogram.types import BotCommand, Message 
from pyrogram.enums import ChatMemberStatus
from tag import Tag
from extra import get_payload
from dotenv import load_dotenv
from asyncio import get_event_loop

load_dotenv()

api_id = getenv('API_ID') or 1
api_hash = getenv('API_HASH') or ''
bot_token = getenv('BOT_TOKEN') or ''
bot = Client('vyu-tag-all-bot', api_id, api_hash, bot_token=bot_token)
tags: Dict[int, Tag] = {}
sudo_users = []
loop = get_event_loop()
prefixes = [".", "/", "#", "@"]

@bot.on_message(filters.group & filters.command(["all", "tag"], prefixes))
async def tag_handler(_, message: Message):
  chat_id = message.chat.id

  try:
    user_id = message.from_user.id
    user = await bot.get_chat_member(chat_id, user_id)

    if user.status != ChatMemberStatus.ADMINISTRATOR and user.status != ChatMemberStatus.OWNER:
      if user_id not in sudo_users: return

    if chat_id in tags: del tags[chat_id]

    tag_message = get_payload(message.text)
    tags[chat_id] = Tag(bot, tag_message, chat_id)

    try: await bot.delete_messages(message.chat.id, message.id)
    except: pass
    
    await tags[chat_id].get_all_users()
    await tags[chat_id].tag_all_users()
  except Exception as err:
    print(err)
    return

@bot.on_message(filters.command("q", prefixes))
async def delete_mention_handler(_, message):
  chat_id = message.chat.id

  if chat_id in tags:
    tags[chat_id].cancel = True
    await bot.delete_messages(chat_id, message.id)
    await tags[chat_id].delete_all_tag_messages()
    del tags[chat_id]

async def init():
  await bot.start()
  botInfo = await bot.get_me()
  print(f"App started as {botInfo.username}")

  await bot.set_bot_commands([
    BotCommand("all", "tag all"),
    BotCommand('q', 'cancel')
  ])
  await idle()

loop.run_until_complete(init())
